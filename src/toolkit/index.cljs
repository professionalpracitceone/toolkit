(ns toolkit.index
  (:require [reagent.dom :as rdom]))

;; -- utils ----------------------------------------
(defn footer []
  [:footer.text-center.h-16.py-2
   "T05-Group 3"
   [:ul.list-none.gap-x-4.inline-flex
    [:li [:a.underline {:href "mailto:u7772256@anu.edu.au"} "Songyuan Guo"]]
    [:li [:a.underline {:href "mailto:u7670058@anu.edu.au"} "Peng Huang"]]
    [:li [:a.underline {:href "mailto:u7671807@anu.edu.au"} "Xiaolong Yan"]]]])

(defn link [{:keys [url text color]}]
  [:a {:href url,
       :target "_blank",
       :class (conj ["underline"] (or color []))}
   text])

(defn toolkit-header [{:keys [text bg-clr]}]
  [:header
   [:h2 text]
   [:div.highlight {:class bg-clr}]])

;; -- Intro ----------------------------------------
(defn company-intro []
  [:section
   [:div.intro
    [:h1 "Toolkit"]
    [:div.flex.text-lg
     [:p.flex-1.px-2
      [:span.italic "SmartContract "] "is a dynamic software company at the forefront of financial technology, specializing in the development of both traditional trading systems and cutting-edge blockchain-based solutions.  With a clear vision for the future of finance, SmartContract combines expertise in traditional financial markets with innovative blockchain technologies, revolutionizing the way financial transactions are executed and managed."]
     [:p.flex-1.px-2
      "In recent years, the company has been benefiting from ethinical engineering practice and has developed a handful of its own tools to educate the software developers in the industry.  Here are "
      [:span.italic "two"]
      " such tools that the company highly recommends.  Tool one aims to teach junior engineers how to identify the key stakeholders; the other one is designed to demonstrate important ethical engieering topics such as effective memebers, responsible innovation and so on."]]]])

;; -- toolkit 1 ------------------------------------
(defn stake-holder-bg []
  [:div
   [:p.my-2 "SmartContract, as a software company developing both traditional trading systems and new blockchain-based systems, has various stakeholders who have an interest in or are affected by its operations. Use this tool to practice finding a list of stakeholders in a given context and categorzing them properly to get a better stakeholder map."]


   [:p.my-2 "When you finish, you may want to refer to the company's "
    [link {:text "version",
           :color "text-rbl",
           :url "https://docs.google.com/spreadsheets/d/1MsXsq5P1dkXLb5-LfeGzTl2FWcSbK2iZcCHwDuCmeHg/edit#gid=698586483"}]
    "."]])

(defn stakeholder-ctx []
  [:div.stake-holder.border-b.border-rpl
   [:p.sh-label "Context"]
   [:p "Last year, the dev team made a serious bug that caused the entire trading system on the blockchain down for 33 hours.  News and reports as well as doubts and complaints were spreading in an uncontroled way.  Third-party plugins or products were also seriously impacted.  The company's share price fell by 15% within 2 hours.  People rushed to banks swapping their crypto currency back with fiat, which caused huge liquidity pressures on small-and-medium banks across the country.  It turned out that many factors contributed to the diaster, inadequent training on new developers, inefficient communication caused frequent conflicts in and outside offices, new features and techs were forced into use without comprehensive tests, leadership leaned towards VIP clients too much ...  Even today the company has not fully recovered its damaged image. The most important lesson learnt, according to then CEO Mike Johnason, is identify the key stakeholders and address their interests properly."]])

(defn stakeholder-prac1 []
  [:div.stake-holder.border-b.border-rpl
   [:p.sh-label "Identify"]
   [:div.prac
    [:p.self-center
     "In this practice, you will need to identify " [:span.italic "at least 10"]
     " stakeholders of SmartContract according to the above background/context.  Some are already given for your reference (see the left list below) "]
    [:p.prac-tips
     [:span.font-bold.text-rpl.mr-2 "Tips"]
     "Think of all the people who have been affected by the system-down diaster, who have influence or power over it or have an interest in it. Remember that although stakeholders may be organizations, in the end you must communicate with people. Therefore, make sure that you identify the correct individual stakeholders within a stakeholder organization."]]])

(defn stakeholder-prac2 []
  [:div.stake-holder
   [:p.sh-label "Classify"]
   [:div
    [:p
     "Once you have a list of the stakeholders, it is important to " [:span.italic "categorize"]
     " them according to their influce and interest.  Doing this helps in identifying stakeholders based on their power and interest in the project.  Those with high influence need to be kept satisfied, while those with high interest need to be kept informed.  When a stakeholder has both, make sure you address their expectations properly!"]
    [:p.font-bold.text-rpl "Hint"]
    [:dl
     [:dt "Hign Influence + High Interest"]
     [:dd "Often the decision makers and have the biggest impact on the project success"]
     [:dt "High Influence + Low Interest"]
     [:dd "Often the people you need to keep in loop even though they aren’t interested in your project or business.  Yet they have influence and they may use it not in favour of your project if they are not satisfied"]
     [:dt "Low Interest + High Interest"]
     [:dd "These people often contribute to or be helpful to your business and you'd better to keep them adequately informaed"]
     [:dt "Low Interest + Low Interest"]
     [:dd "These people are loosely related to your projects but it's good to keep an eye on them but do not bore them with excessive communication"]]]])

(defn stakeholder-list []
  [:div.flex-1.flex.py-10
   ;; list labels
   [:div.mr-2
    [:p "\u2776"]
    [:p "\u2777"]
    [:p "\u2778"]
    [:p "\u2779"]
    [:p "\u277A"]
    [:p "\u277B"]
    [:p "\u277C"]
    [:p "\u277D"]
    [:p "\u277E"]
    [:p "\u277F"]]
   ;; list content
   [:div.ml-2
    [:p "All users of the trading system"]
    [:p "Blockchain users"]
    [:p "Employees of SmartContract"]]])

(defn stakeholder-chart []
  [:div.chart
   ;; labels
   [:p.low-inf "Low Influence"]
   [:p.high-inf "High Influence"]
   [:p.high-int "High Interest"]
   [:p.low-int "Low Interest"]
   ;; left-top
   [:div.border-r.border-b.abosulte.top-0.left-0]
   ;; right-top
   [:div.border-l.border-b.absolute.top-0.right-0]
   ;; left-bottom
   [:div.border-r.border-t.absolute.bottom-0.left-0]
   ;; right-bottom
   [:div.border-l.border-t.absolute.bottom-0.right-0]])

(defn stake-holder []
  [:section.pale-purple
   [:div.t-one
    [toolkit-header {:text   "Tool One",
                     :bg-clr "bg-rpl"}]
    [stake-holder-bg]
    [stakeholder-ctx]
    [stakeholder-prac1]
    [stakeholder-prac2]
    [:div.flex.justify-between
     [stakeholder-list]
     [stakeholder-chart]]]])

;; -- toolkit 2 ------------------------------------
(defn game-intro []
  [:div
   [:p.my-2
    "The software developement team in SmartContract has desiged a card game, particularly targeting software developers of financial/trading systems."]
   [:p.my-2
    "This game aims to educate these developers, junior or senior alike, on various ethnical engieering topics such as how to become an effective team member, what does it mean to be an responsible innovator, how important it is to write and present effectively."]])


(defn game-cards []
  [:div
   [:div.game-card
    [:p.game-label "90"]
    [:p "White task cards.  Each card has a specific task with a difficulty level on it, from 1 star (easy) to 5 stars (hard)"]]
   [:div.game-card
    [:p.game-label "15"]
    [:p "Black ethical cards.  On one side of each card, there is a pratice that violates engieering ethics; on the other side, it shows the relevant topic in engieerning ethics"]]
   [:div.game-card
    [:p.game-label "15"]
    [:p "Blue ethical cards.  On one side of each card, there is a pratice that encourage ethical engieerning practice; on the other side, it shows the relevant topics in engieerning ethics"]]])

(defn game-components []
  [:div.flex-1
   [:img.icon-img {:src "/images/card-games.png",
          :alt "Cards icon"}]
   [:h3 "Components"]
   [game-cards]])

(defn game-setup []
  [:div.flex-1
   [:img.icon-img {:src "/images/shuffle.png",
          :alt "Cards icon"}]
   [:h3 "Set up"]
   [:div
    [:p [:span.game-label "2-4"] " players"]
    [:p [:span.game-label "10"]" cards for each player"]
    [:p [:span.game-label "5"] " card groups.  The 10 cards each player gets must contain 2 cards from 1-star tasks,  2 cards from 2-star tasks and so forth"]]])

(defn game-rules []
  [:div.flex-1
   [:img.icon-img {:src "/images/how-to.png",
          :alt "Cards icon"}]
   [:h3 "How to play"]
   [:div
    [:div.game-play
     [:p.game-label "Turn"]
     [:p "Players can use a die or anything simiar to decide who acts first and others act in a clockwise direction.  A player can act once in his/her turn."]]
    [:div.game-play
     [:p.game-label "Task"]
     [:p "In each turn, the player can choose to work on current task or a new task and thus will get one star from that task.  Each turn one player can get one star only.  A 5-star tasks will required 5 turns of the player."]]
    [:div.game-play
     [:p.game-label "Slow"]
     [:p "Alternatively, the player can draw a black card to impede any other players.  The black-card-attacked player has to stop act in his/her turn, as if s/he were to address the issue described in the black card."]]
    [:div.game-play
     [:p.game-label "Help"]
     [:p "Each time when a player gets 3 stars, s/he can select a blue card to cancel the current or future negative impact on his/her current task imposed by a black card.  If a player's blue card addresses exactly the issue from the black card attack, the player cancels that attack and also gets one star in this turn.  If blue card handles a different issue from that on the black card, the black-card impact is canceled only.  Player will not get the extra one star. In addition, stars cannot be resued.  For example, a player is eligible for blue cards only when s/he gets 3 stars, 6 stars, and so on.  A player can act regardless of using blue cards."]]]])

(defn game-explanation []
  [:div.game
   ;; Components
   [game-components]
   ;; Setup
   [game-setup]
   ;; Rule
   [game-rules]])

(defn game-scoring []
  [:div.game
   [:p "The game continues until one player finishes all his/her tasks.  The complete information of the abovementioned cards can be found in this "
    [link {:url "https://docs.google.com/spreadsheets/d/1MsXsq5P1dkXLb5-LfeGzTl2FWcSbK2iZcCHwDuCmeHg/edit#gid=0",
           :text "Google Sheet.",
           :color "text-rbl"}
     ]]])

(defn game []
  [:section.pale-green
   [:div.t-two
    [toolkit-header {:text "Tool Two",
                     :bg-clr "bg-cbl"}]
    [game-intro]
    [game-explanation]
    [game-scoring]]])


(defn layout []
  [:div.layout
   [company-intro]
   [stake-holder]
   [game]
   [footer]])

(defn mount! []
  (rdom/render [layout]
    (.getElementById js/document "app")))

(defn -main []
  (mount!))

(defn after-load []
  (mount!))
