module.exports = {
  content: {
    files: ['./src/toolkit/*.cljs'],
  },
  theme: {
    extend: {
      flex: {
        '1-1': '1 1',
      },
      colors: {
        "ryl": '#FFC832',
        "rgr": '#19A974',
        "rpl": '#2E2459',
        "rbl": '#4299BF',
        'cgr': '#DFE9EB',
        'cdg': '#315D68',
        'cbl': '#53A7B8',
        'lpr': '#E9E3F7',
      }
    }
  }
}
