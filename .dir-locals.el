;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((clojure-mode . ((cider-shadow-watched-builds . ("index"))
                  (cider-preferred-build-tool . shadow-cljs)
                  (cider-default-cljs-repl . shadow))))
