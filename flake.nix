{
  description = "An over-engineered static site for project landpage";

  inputs = {
    korin.url = "github:Linerre/korin";
    nixpkgs.follows = "korin/nixpkgs";
    flake-utils.follows = "korin/flake-utils";
  };

  outputs = { self, flake-utils, nixpkgs, ... }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
        {
          devShells.default = pkgs.mkShell {

            buildInputs = with pkgs; [
              jdk17_headless
              nodejs_18
              clojure
              clojure-lsp
              just
            ];


            shellHook = ''
              alias e="emacs -nw"
              alias ll="ls -alh"
            '';
          };
        }
    );

  # Optional

}
